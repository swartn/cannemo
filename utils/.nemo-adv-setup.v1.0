#!/bin/bash
# Default setup script for nemo only mode:
#   - gets sourced by setup-nemo after it clones down the CanNEMO repo
#   - all inherited variables come from setup-nemo
#   - the bail function is inherited from setup-nemo
#
# Note that this 'default' file is only used for "old" runs, which are setup
#   from commits that do not have 'nemo-adv-setup'
#======================================================================

# Set up maestro according the jobtype
if [[ "$jobtype" == 'rtd' ]] || [[ "$jobtype" == 'dumpdel' ]]; then

    if [ "$jobtype" == 'rtd' ]; then
        # set up dirs for rtd
        echo "Setting up an rtd only job"
        # if the use gave a maestro version, use that, else default to 
        # rtd only branch
        [ $usr_maestro_ver -eq 1 ] || maestro_version='maestro-rtd'
    else
        # set up dirs for dumpdel
        echo "Setting up a dump(to tape)/delete data only job"
        # if user gave a maestro version, use that, else default to the 
        # hpcarchonly branch
        [ $usr_maestro_ver -eq 1 ] || maestro_version='maestro-hpcarch'
    fi

    # create directory by cloning the maestro repo
    [ -d ${nemo_runid}-${jobtype} ] && bail "Directory ${nemo_runid}-${jobtype} already exists! Aborting"
    git clone $maestro_repo ${nemo_runid}-${jobtype}
    cd ${nemo_runid}-${jobtype} || { echo "Failed to clone" ;  exit 1 ; }
    git checkout $maestro_version

    CWD=$(pwd)
    nrg=$(pwd)/.git
    sha1_maestro=$(git --git-dir=${nrg} rev-parse ${maestro_version})

    # basic maestro setup
    export SEQ_EXP_HOME=$(pwd)
    mkdir hub logs sequencing stats listings
    makelinks -f
    PATH=$CWD/bin:$PATH
    export PATH

    sed -i "s/RUNID=.*/RUNID=${nemo_runid}/g" experiment.cfg
    sed -i "s/SEQ_JOBNAME_PREFIX=.*/SEQ_JOBNAME_PREFIX=${nemo_runid}-/g" resources/resources.def
    sed -i "s/CONFIG=.*/CONFIG=${nemo_config}/g" experiment.cfg
    config-maestro

    # produce message about experiement.cfg
    echo ""
    if [ "$jobtype" == 'rtd' ]; then 
        echo "NOTE: You should copy the experiment.cfg used for the run here, then do bin/config-maestro before launching."
        echo "Alter the 'start' and 'end' variables if you wish to only perform the diagnostics on a sub-range."
    else
        echo "NOTE: You should copy the experiment.cfg used for the run here, and then alter the settings to fit your needs."
        echo "Warning: it is possible to delete data that hasn't been archived to tape. Be sure to review the descriptions for 'Dump to tape settings' and 'Delete off sitestore settings'."
        echo "See http://wiki.cccma.ec.gc.ca/twiki/bin/view/Main/TechnicalDevelopmentConversion2017-nemo-maestro-instructions for additional details"
    fi

    # print to log
    cd ../
    echo '~~~' >> .setup-${jobtype}.log
    echo `date` >> .setup-${jobtype}.log
    echo "setup-${jobtype}"  $@ >> .setup-${jobtype}.log
    echo 'Maestro suite corresponds to SHA1:' $sha1_maestro >> .setup-${jobtype}.log
else
    echo "Setting up an ocean only run"
    [ -z "$nemo_config" ] &&
        bail "The name of the configuration is required on the command line ...config=cfgname"

    # Try to get user input list for a new config.
    get_list(){
    echo " "
    echo "*** Unrecognized CONFIG! ***"
    echo " "
    echo "This might be OK if you have just added this CONFIG to your repo, otherwise you are in trouble and should abort.
          We need to figure out which namelists (etc) you want to save from this config. The choices below relate only 
          to the *names* of files to be used. The files themselves will be extracted from the CONFIG ($nemo_config) that you specified."
    echo " "
    echo "Choose an option and hit enter"
    echo "
          a) Abort \n 
          b) use physics default: 'namelist namelist_ice iodef.xml xmlio_server.def' \n
          c) use carbon (pisces/canoe) default: 'namelist namelist_ice namelist_pisces namelist_top iodef.xml xmlio_server.def' \n
          d) use carbon (cmoc) default: 'namelist namelist_ice namelist_pisces namelist_cmoc namelist_top iodef.xml xmlio_server.def'
         "
    echo " "
    read inlist
    case $inlist in
        a) bail "Aborting..." ;;
        b) lists='namelist namelist_ice namelist_top iodef.xml xmlio_server.def' ;;
        c) lists='namelist namelist_ice namelist_pisces namelist_top iodef.xml xmlio_server.def'  ;;
        d) lists='namelist namelist_ice namelist_pisces namelist_cmoc namelist_top iodef.xml xmlio_server.def' ;;
        *) bail "Aborting" ;;
    esac
    }

    # Make sure the config is valid and decide which namelist files we need.
    case $nemo_config in
        CCC_ORCA025_LIM)      lists='namelist namelist_ice namelist_top iodef.xml xmlio_server.def'                               ;;
        CCC_ORCA025_LIM_CMOC) lists='namelist namelist_ice namelist_pisces namelist_cmoc namelist_top iodef.xml xmlio_server.def' ;;
        CCC_ORCA1_LIM)        lists='namelist namelist_ice namelist_top iodef.xml xmlio_server.def'                               ;;
        CCC_ORCA1_LIM_PISCES) lists='namelist namelist_ice namelist_pisces namelist_top iodef.xml xmlio_server.def'  ;;
        CCC_ORCA1_LIM_CANOE) lists='namelist namelist_ice namelist_pisces namelist_top iodef.xml xmlio_server.def'  ;;
        CCC_ORCA1_LIM_CMOC)   lists='namelist namelist_ice namelist_pisces namelist_cmoc namelist_top iodef.xml xmlio_server.def' ;;
        CCC_ORCA1_OFF_PISCES) lists='namelist namelist_pisces namelist_top iodef.xml xmlio_server.def'  ;;
        CCC_ORCA1_OFF_CANOE) lists='namelist namelist_pisces namelist_top iodef.xml xmlio_server.def'  ;;
        CCC_ORCA1_OFF_CMOC) lists='namelist namelist_pisces namelist_top namelist_cmoc iodef.xml xmlio_server.def'  ;;
                         #*) bail "Invalid configuration --> $arg <-- ...config MUST be a CCC_* config." ;;
                         *) get_list ;;
    esac

    nemo_config_lc=$(echo "$nemo_config" | awk '{print tolower($0)}')

    #========================================================================
    # Make a directory by cloning the maestro repo and do basic maestro setup
    #========================================================================
    [ -d $nemo_runid ] && bail "Directory $nemo_runid already exists! Aborting"

    # clone
    git clone $maestro_repo $nemo_runid
    cd $nemo_runid || { echo "Failed to create run directory" ;  exit 1 ; }
    git checkout $maestro_version
    CWD=$(pwd)
    nrg=$(pwd)/.git
    sha1_maestro=$(git --git-dir=${nrg} rev-parse ${maestro_version})

    # basic maestro setup
    export SEQ_EXP_HOME=$(pwd)
    mkdir hub logs sequencing stats listings
    touch "logs/$(date '+%Y%m%d')000000_nodelog"
    makelinks -f
    PATH=$CWD/bin:$PATH
    export PATH

    # loop over hosts in host/input-path pairs and set them inside ${SEQ_EXP_HOME}/hub/${host}
    if [[ -n "$input_paths" ]]; then
        for hostpath in $input_paths; do
            host=${hostpath%:*}
            path=${hostpath#*:}
            ssh $host "export SEQ_EXP_HOME=$SEQ_EXP_HOME; export TRUE_HOST=$host; ${SEQ_EXP_HOME}/bin/set-inputs $path"
        done
    fi

    # link in the nemo source code
    ln -s ${src_dir}

    # Get the exact checksum of the ver
    nrg=${src_dir}/.git
    nemo_sha1=$(git --git-dir=${nrg} rev-parse ${nemo_version})
    echo "Using nemo sha: $nemo_sha1"

    # copy experiment.cfg to maestro directory
    cp -p -f ${src_dir}/nemo/CONFIG/${nemo_config}/EXP00/experiment.cfg ${CWD}/.
    
    # set experiment specific values in config and config maestro
    sed -i "s#^storage_dir=.*#storage_dir=${storage_dir}#g" experiment.cfg
    sed -i "s/RUNID=.*/RUNID=${nemo_runid}/g" experiment.cfg
    sed -i 's/.*config=.*/config='"$nemo_config/" experiment.cfg
    if [ ${with_nemo_diag} = on ] ; then
       sed -i "s/with_nemo_diag=.*/with_nemo_diag=on/" experiment.cfg
    fi
    sed -i "s/SEQ_JOBNAME_PREFIX=.*/SEQ_JOBNAME_PREFIX=${nemo_runid}-/g" resources/resources.def
    config-maestro

    # Get the compile script, and reset runid, repo and ver.
    ln -s ${src_dir}/nemo/CONFIG/${nemo_config}/EXP00/compile ${CWD}/compile
    [ -e compile ] || bail "Failed to extract compile script from ${src_dir}/nemo/CONFIG/${nemo_config}/EXP00/compile" 
    cat compile | sed "s#build-nemo#./CanNEMO_tmp_src/bin/build-nemo#;s#runid#${nemo_runid}#;s#repo=[^ ]*#repo="$nemo_repo"#;s#ver=[^ ]*#ver=${nemo_sha1} srcpath=${src_dir}#" > compile_${nemo_runid}
    if [ ${with_nemo_diag} = off ] ; then
       sed -i '/cfg/s/$/ del_key="key_trdtra key_diaar5"/' compile_${nemo_runid}
    fi
    sed -i '/build-nemo/s/$/ > .compile-cannemo-$$.log 2>\&1 \&/' compile_${nemo_runid}
    rm -f compile
    chmod +x compile_${nemo_runid}

    # add to compile script to allow it to be called from any where (not just on backends)
    insrt_hdr="#!/bin/bash\nCWD="'`pwd`'"\nssh "'$(whoami)'"@daley << EOSSH\ncd "'$CWD'"\n"
    sed -i "1i $insrt_hdr" compile_${nemo_runid}
    echo "EOSSH" >> compile_${nemo_runid}

    nemo_config_lower=$(echo ${nemo_config} | tr '[:upper:]' '[:lower:]')
    nemo_exe=${nemo_runid}_${nemo_config_lower}_exe
    sed -i "s#nemo_exec=.*#nemo_exec="'${SEQ_EXP_HOME}/'${nemo_exe}"#g" experiment.cfg

    # SETUP links to relevant namelists
    # Get the namelists/iodefs
    if [ -s $CWD/namelists ]; then 
        echo "WARNING: namelists dir already exists, overwriting"
        cd $CWD/namelists
    else
        mkdir namelists
        cd $CWD/namelists
    fi
    . ${CWD}/bin/CanNEMO_shell_functions.sh
    for i in $lists
    do
        # Link here from source using relative links
        ln -s ${src_dir}/nemo/CONFIG/${nemo_config}/EXP00/${i} .
        if [[ $i == *"namelist"* ]]; then
           sed -i "s#${i}=.*#${i}="'${SEQ_EXP_HOME}/namelists/'${i}"#" ../experiment.cfg
        fi
    done
    sed -i "s#nemo_xmlio_server_def=.*#nemo_xmlio_server_def="'${SEQ_EXP_HOME}/namelists/'xmlio_server.def"#" ../experiment.cfg
    sed -i "s#nemo_iodef=.*#nemo_iodef="'${SEQ_EXP_HOME}/namelists/'iodef.xml"#" ../experiment.cfg

    # Setup files and carbon options, depending on the config.
    if [[ $nemo_config_lower == *"cmoc"* ]] || [[ $nemo_config_lower == *"canoe"* ]]; then
        sed -i "s#nemo_carbon=.*#nemo_carbon=on#" ../experiment.cfg
        if [[ $nemo_config_lower == *"cmoc"* ]]; then
            sed -i "s#nemo_cmoc=.*#nemo_cmoc=on#" ../experiment.cfg
        fi
        if [[ $nemo_config_lower == *"off"* ]]; then
            sed -i 's#nemo_hist_file_suffix_list=.*#nemo_hist_file_suffix_list="ptrc_T diad_T"#' ../experiment.cfg
            sed -i 's#nemo_hist_file_freq_list=.*#nemo_hist_file_freq_list="1m     1m"#' ../experiment.cfg
            sed -i 's#nemo_dump_file_suff=.*#nemo_dump_file_suff="ptrc_T diad_T"#' ../experiment.cfg
            sed -i 's#nemo_dump_file_freq=.*#nemo_dump_file_freq="1m     1m"#' ../experiment.cfg
            sed -i 's#nemo_del_file_suff=.*#nemo_del_file_suff="ptrc_T diad_T"#' ../experiment.cfg
            sed -i 's#nemo_del_file_freq=.*#nemo_del_file_freq="1m     1m"#' ../experiment.cfg
        fi
    fi

    # Set the nemoval resolution, depending on the config (default is 1d in experiment.cfg).
    if [[ $nemo_config_lower == *"orca025"* ]]; then
        sed -i "s#nemoval_res=.*#nemoval_res=025d#" ../experiment.cfg
    fi

    # Set nemo_freq
    echo "Setting nemo_freq, based off given config, to recommended value for 3hr wallclock time"
    case $nemo_config_lower in 
        "ccc_orca1_lim")        frq="10y"   ;; 
        "ccc_orca1_lim_cmoc")   frq="5y"    ;;
        "ccc_orca1_lim_canoe")  frq="2y"    ;;
        "ccc_orca025_lim")      frq="4m"    ;;
        *) 
            echo "There is currently no recommended nemo_freq for config ${nemo_config}"
            echo "Value set to 1y. Alter if desired"
            frq="1y" ;;
    esac
    if [[ ${with_nemo_diag} = on ]] && [[ $nemo_config_lower != *"orca025"* ]]; then frq="1y"; fi
    sed -i "s#nemo_freq=.*#nemo_freq=${frq}#" ../experiment.cfg

    # print to log
    cd ../
    echo '~~~' >> .setup-nemo.log
    echo `date` >> .setup-nemo.log
    echo 'setup-nemo ' $@ >> .setup-nemo.log

    echo 'Code corresponds to SHA1:' $nemo_sha1 >> .setup-nemo.log
    echo 'Maestro suite corresponds to SHA1:' $sha1_maestro >> .setup-nemo.log

    # Write log to nemo run database
    datef=$(date +"%Y-%m-%d %T")
    userf=$(whoami)

    #sqlite3 /home/ncs001/ocean_runs/ocean_runs.db  << EOF
    #                        INSERT INTO runs (runid, date, user, config, ver, repo, sha1)
    #                        values('${nemo_runid}', '${datef}', '${userf}', '${nemo_config}', 
    #                               '${nemo_version}', '${nemo_repo}', '${sha1}');
#EOF

    # if the -l flag was given, launch simulation according to the desired length
    if (( launch_job == 1 )); then
        echo "Compiling and launching default $nemo_config simulation from 0001:01 to ${rn_ln_y}:${rn_ln_m}"

        # compile
        echo "compiling..."
        ./compile_${runid} >> compile.log 2>&1    

        # considerations for quarter degree model
        if [[ $nemo_config_lower == *"orca025"* ]]; then
            sed -i "s#nemo_rtd=.*#nemo_rtd=off#" ./experiment.cfg

            # set necessary data files for added resolution
            # grid information
            sed -i "s#orca_grid_info=.*#orca_grid_info=nemo3.4.1_orca025_mesh_mask_ngb.nc#" ./experiment.cfg
            # init files
            sed -i "s#nemo_data_1m_potential_temperature_nomask=.*#nemo_data_1m_potential_temperature_nomask=nemo_3.4_orca025_potemp_1m_z46_nomask_orca1jan.nc#" ./experiment.cfg
            sed -i "s#nemo_data_1m_salinity_nomask=.*#nemo_data_1m_salinity_nomask=nemo_3.4_orca025_salin_1m_z46_nomask_orca1jan.nc#" ./experiment.cfg
            sed -i "s#nemo_coordinates=.*#nemo_coordinates=nemo_3.4_orca025_coordinates_cmc.nc#" ./experiment.cfg
            sed -i "s#nemo_bathy_meter=.*#nemo_bathy_meter=nemo_3.4_orca025_bathy_meter_cmc.nc#" ./experiment.cfg
            sed -i "s#nemo_M2rowdrg=.*#nemo_M2rowdrg=nemo_3.4.1_orca025_m2rowdrg_orca1_inter.nc#" ./experiment.cfg
            sed -i "s#nemo_K1rowdrg=.*#nemo_K1rowdrg=nemo_3.4.1_orca025_k1rowdrg_orca1_inter.nc#" ./experiment.cfg
            sed -i "s#nemo_sss_data=.*#nemo_sss_data=nemo_3.4_orca025_sss_1m.nc#" ./experiment.cfg
            sed -i "s#nemo_chlorophyll=.*#nemo_chlorophyll=nemo_3.4.1_orca025_chlorophyll_1m_orca1.nc#" ./experiment.cfg
            # forcing consideration (set up to work with default forcing)
            #   - the addressing forces replacements only in default forcing section
            sed -i "/^\s*#/! s#nemo_weights_bicubic2=.*#nemo_weights_bicubic2=uncs_orca025_canesm2_weights_bicubic.nc#" ./experiment.cfg
            sed -i "/^\s*#/! s#nemo_weights_bilinear2=.*#nemo_weights_bilinear2=uncs_orca025_canesm2_weights_bilin.nc#" ./experiment.cfg
            sed -i "/^\s*#/! s#nemo_runoff_core_monthly=.*#nemo_runoff_core_monthly=nemo_3.4.1_orca025_rout_socoefr_month_r1i1p1_1979-2005-mean2.nc#" ./experiment.cfg
            sed -i "/^\s*sn_cnf=./ s#socoeff#socoefr#" ./experiment.cfg
            sed -i "/^\s*sn_cnf=./ s#weights_bil2##" ./experiment.cfg
            # timestep
            sed -i "s#nemo_rdt=.*#nemo_rdt=1440#" ./experiment.cfg
        fi

        # set run length
        sed -i "s#end=.*#end=${rn_ln_y}:${rn_ln_m}#" ./experiment.cfg

        # config maestro
        ./bin/config-maestro 2>/dev/null || 
        bail "your desired simulation length must be set such that nemo_freq (set to ${frq}) "\
        "evenly divides into the number of months in the run. Alter 'end' or 'nemo_freq' in "\
        "experiment.cfg and try to launch again with the following command './bin/config-maestro; "\
        "expbegin -e \`pwd\` -d \`date +%Y%m%d%H%M\`'"

        # launch run
        expbegin -e $(pwd) -d $(date +%Y%m%d%H%M)
    fi
fi
